# Exercise2
Ознакомление с базовыми возможностями


#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

Код:

{
    "tasks": [
        {
            "type": "che",
            "label": "HelloWorld build and run",
            "command": "javac -sourcepath /projects/HelloWorld/src -d /projects/HelloWorld/bin /projects/HelloWorld/src/*.java && java -classpath /projects/HelloWorld/bin HelloWorld",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/HelloWorld/src",
                "component": "maven"
            }
        }
    ]
}

